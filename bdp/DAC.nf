﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(DAC))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(DAC))==(Machine(DAC));
  Level(Machine(DAC))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(DAC)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(DAC))==(?)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(DAC))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(DAC))==(FileSystem);
  List_Includes(Machine(DAC))==(FileSystem)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(DAC))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(DAC))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(DAC))==(?);
  Context_List_Variables(Machine(DAC))==(?);
  Abstract_List_Variables(Machine(DAC))==(?);
  Local_List_Variables(Machine(DAC))==(cur_user,in_group,permission,group,user,owner);
  List_Variables(Machine(DAC))==(cur_user,in_group,permission,group,user,owner,position,wopened,ropened,closed,contents,directories,files,objectTree);
  External_List_Variables(Machine(DAC))==(cur_user,in_group,permission,group,user,owner,position,wopened,ropened,closed,contents,directories,files,objectTree)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(DAC))==(?);
  Abstract_List_VisibleVariables(Machine(DAC))==(?);
  External_List_VisibleVariables(Machine(DAC))==(?);
  Expanded_List_VisibleVariables(Machine(DAC))==(?);
  List_VisibleVariables(Machine(DAC))==(?);
  Internal_List_VisibleVariables(Machine(DAC))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(DAC))==(btrue);
  Gluing_List_Invariant(Machine(DAC))==(btrue);
  Abstract_List_Invariant(Machine(DAC))==(btrue);
  Expanded_List_Invariant(Machine(DAC))==(files <: OBJECT & directories <: OBJECT & objectTree: directories\/files --> directories & files/\directories = {} & contents: files --> seq(BYTE) & closed <: files & ropened <: files & wopened <: files & closed/\ropened = {} & closed/\wopened = {} & wopened/\ropened = {} & closed\/ropened\/wopened = files & position: ropened --> NAT1);
  Context_List_Invariant(Machine(DAC))==(btrue);
  List_Invariant(Machine(DAC))==(user <: UG & group <: UG & user/\group = {} & owner: directories\/files --> user & permission: (user\/group)*(directories\/files) +-> POW(RIGHT) & !obj.(obj: directories\/files => permission(owner(obj)|->obj) = {RR,WW}) & !obj.(obj: directories\/files => permission(su|->obj) = {RR,WW}) & not(#obj.(obj: directories\/files & null|->obj: dom(permission))) & in_group: user <-> group & cur_user: user & su: user & null: user)
END
&
THEORY ListAssertionsX IS
  Abstract_List_Assertions(Machine(DAC))==(btrue);
  Expanded_List_Assertions(Machine(DAC))==(btrue);
  Context_List_Assertions(Machine(DAC))==(btrue);
  List_Assertions(Machine(DAC))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(DAC))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(DAC))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(DAC))==(files,directories,objectTree,contents,closed,ropened,wopened,position:={},{root},{root|->root},{},{},{},{},{};owner,user,group,permission,in_group,cur_user:={root|->su},{su,null},{},{su|->root|->{RR,WW}},{},null);
  Context_List_Initialisation(Machine(DAC))==(skip);
  List_Initialisation(Machine(DAC))==(owner:={root|->su} || user:={su,null} || group:={} || permission:={su|->root|->{RR,WW}} || in_group:={} || cur_user:=null)
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(DAC))==(?)
END
&
THEORY ListInstanciatedParametersX IS
  List_Instanciated_Parameters(Machine(DAC),Machine(FileSystem))==(?)
END
&
THEORY ListConstraintsX IS
  List_Constraints(Machine(DAC),Machine(FileSystem))==(btrue);
  List_Context_Constraints(Machine(DAC))==(btrue);
  List_Constraints(Machine(DAC))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(DAC))==(login,logout,create_user,delete_user,create_group,delete_group,add_group,remove_group,grant_permission,revoke_permission,dac_create_file,dac_create_dir,dac_move,dac_copy,dac_delete,dac_ropen,dac_wopen,dac_close,dac_read,dac_write);
  List_Operations(Machine(DAC))==(login,logout,create_user,delete_user,create_group,delete_group,add_group,remove_group,grant_permission,revoke_permission,dac_create_file,dac_create_dir,dac_move,dac_copy,dac_delete,dac_ropen,dac_wopen,dac_close,dac_read,dac_write)
END
&
THEORY ListInputX IS
  List_Input(Machine(DAC),login)==(uu);
  List_Input(Machine(DAC),logout)==(?);
  List_Input(Machine(DAC),create_user)==(uu);
  List_Input(Machine(DAC),delete_user)==(uu);
  List_Input(Machine(DAC),create_group)==(gg);
  List_Input(Machine(DAC),delete_group)==(gg);
  List_Input(Machine(DAC),add_group)==(uu,gg);
  List_Input(Machine(DAC),remove_group)==(uu,gg);
  List_Input(Machine(DAC),grant_permission)==(ug,obj,pp);
  List_Input(Machine(DAC),revoke_permission)==(ug,obj,pp);
  List_Input(Machine(DAC),dac_create_file)==(file,parent_dir);
  List_Input(Machine(DAC),dac_create_dir)==(dir,parent_dir);
  List_Input(Machine(DAC),dac_move)==(obj,destDir);
  List_Input(Machine(DAC),dac_copy)==(obj,destDir);
  List_Input(Machine(DAC),dac_delete)==(obj);
  List_Input(Machine(DAC),dac_ropen)==(file);
  List_Input(Machine(DAC),dac_wopen)==(file);
  List_Input(Machine(DAC),dac_close)==(file);
  List_Input(Machine(DAC),dac_read)==(file);
  List_Input(Machine(DAC),dac_write)==(file,data)
END
&
THEORY ListOutputX IS
  List_Output(Machine(DAC),login)==(?);
  List_Output(Machine(DAC),logout)==(?);
  List_Output(Machine(DAC),create_user)==(?);
  List_Output(Machine(DAC),delete_user)==(?);
  List_Output(Machine(DAC),create_group)==(?);
  List_Output(Machine(DAC),delete_group)==(?);
  List_Output(Machine(DAC),add_group)==(?);
  List_Output(Machine(DAC),remove_group)==(?);
  List_Output(Machine(DAC),grant_permission)==(?);
  List_Output(Machine(DAC),revoke_permission)==(?);
  List_Output(Machine(DAC),dac_create_file)==(?);
  List_Output(Machine(DAC),dac_create_dir)==(?);
  List_Output(Machine(DAC),dac_move)==(?);
  List_Output(Machine(DAC),dac_copy)==(cp);
  List_Output(Machine(DAC),dac_delete)==(?);
  List_Output(Machine(DAC),dac_ropen)==(?);
  List_Output(Machine(DAC),dac_wopen)==(?);
  List_Output(Machine(DAC),dac_close)==(?);
  List_Output(Machine(DAC),dac_read)==(byte);
  List_Output(Machine(DAC),dac_write)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(DAC),login)==(login(uu));
  List_Header(Machine(DAC),logout)==(logout);
  List_Header(Machine(DAC),create_user)==(create_user(uu));
  List_Header(Machine(DAC),delete_user)==(delete_user(uu));
  List_Header(Machine(DAC),create_group)==(create_group(gg));
  List_Header(Machine(DAC),delete_group)==(delete_group(gg));
  List_Header(Machine(DAC),add_group)==(add_group(uu,gg));
  List_Header(Machine(DAC),remove_group)==(remove_group(uu,gg));
  List_Header(Machine(DAC),grant_permission)==(grant_permission(ug,obj,pp));
  List_Header(Machine(DAC),revoke_permission)==(revoke_permission(ug,obj,pp));
  List_Header(Machine(DAC),dac_create_file)==(dac_create_file(file,parent_dir));
  List_Header(Machine(DAC),dac_create_dir)==(dac_create_dir(dir,parent_dir));
  List_Header(Machine(DAC),dac_move)==(dac_move(obj,destDir));
  List_Header(Machine(DAC),dac_copy)==(cp <-- dac_copy(obj,destDir));
  List_Header(Machine(DAC),dac_delete)==(dac_delete(obj));
  List_Header(Machine(DAC),dac_ropen)==(dac_ropen(file));
  List_Header(Machine(DAC),dac_wopen)==(dac_wopen(file));
  List_Header(Machine(DAC),dac_close)==(dac_close(file));
  List_Header(Machine(DAC),dac_read)==(byte <-- dac_read(file));
  List_Header(Machine(DAC),dac_write)==(dac_write(file,data))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(DAC),login)==(uu: user & cur_user = null);
  List_Precondition(Machine(DAC),logout)==(cur_user/=null);
  List_Precondition(Machine(DAC),create_user)==(cur_user = su & uu: UG & uu/:user & uu/:group);
  List_Precondition(Machine(DAC),delete_user)==(cur_user = su & uu: user & uu/=su & uu/=null);
  List_Precondition(Machine(DAC),create_group)==(cur_user = su & gg: UG & gg/:group & gg/:user);
  List_Precondition(Machine(DAC),delete_group)==(cur_user = su & gg: group);
  List_Precondition(Machine(DAC),add_group)==(cur_user = su & gg: group & uu: user & gg/:in_group[{uu}] & uu/=null);
  List_Precondition(Machine(DAC),remove_group)==(cur_user = su & gg: group & uu: user & gg: in_group[{uu}] & uu/=null);
  List_Precondition(Machine(DAC),grant_permission)==(cur_user = su & ug: user\/group & obj: directories\/files & pp: RIGHT & ug/=null);
  List_Precondition(Machine(DAC),revoke_permission)==(cur_user = su & ug: user\/group & obj: directories\/files & pp: RIGHT & ug|->obj: dom(permission) & pp: permission(ug|->obj) & ug/=su & owner(obj)/=ug & ug/=null);
  List_Precondition(Machine(DAC),dac_create_file)==(file: OBJECT & file/:files & file/:directories & parent_dir: directories & (cur_user|->parent_dir: dom(permission) & WW: permission(cur_user|->parent_dir) or #gg.(gg: in_group[{cur_user}] & gg|->parent_dir: dom(permission) & WW: permission(gg|->parent_dir))));
  List_Precondition(Machine(DAC),dac_create_dir)==(dir: OBJECT & dir/:files & dir/:directories & parent_dir: directories & (cur_user|->parent_dir: dom(permission) & WW: permission(cur_user|->parent_dir) or #gg.(gg: in_group[{cur_user}] & gg|->parent_dir: dom(permission) & WW: permission(gg|->parent_dir))));
  List_Precondition(Machine(DAC),dac_move)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & destDir/=obj & (cur_user|->destDir: dom(permission) & WW: permission(cur_user|->destDir) or #gg.(gg: in_group[{cur_user}] & gg|->destDir: dom(permission) & WW: permission(gg|->destDir))) & (cur_user|->obj: dom(permission) & RR: permission(cur_user|->obj) or #gg.(gg: in_group[{cur_user}] & gg|->obj: dom(permission) & RR: permission(gg|->obj))));
  List_Precondition(Machine(DAC),dac_copy)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & (cur_user|->destDir: dom(permission) & WW: permission(cur_user|->destDir) or #gg.(gg: in_group[{cur_user}] & gg|->destDir: dom(permission) & WW: permission(gg|->destDir))) & (cur_user|->obj: dom(permission) & RR: permission(cur_user|->obj) or #gg.(gg: in_group[{cur_user}] & gg|->obj: dom(permission) & RR: permission(gg|->obj))));
  List_Precondition(Machine(DAC),dac_delete)==(obj: files\/directories & tcl(objectTree~)[{obj}]\/{obj}/\files <: closed & owner(obj) = cur_user);
  List_Precondition(Machine(DAC),dac_ropen)==(file: files & file: closed & (cur_user|->file: dom(permission) & RR: permission(cur_user|->file) or #gg.(gg: in_group[{cur_user}] & gg|->file: dom(permission) & RR: permission(gg|->file))));
  List_Precondition(Machine(DAC),dac_wopen)==(file: files & file: closed & (cur_user|->file: dom(permission) & WW: permission(cur_user|->file) or #gg.(gg: in_group[{cur_user}] & gg|->file: dom(permission) & WW: permission(gg|->file))));
  List_Precondition(Machine(DAC),dac_close)==(file: files & file: ropened\/wopened);
  List_Precondition(Machine(DAC),dac_read)==(file: files & file: ropened & contents(file)/=<> & (cur_user|->file: dom(permission) & RR: permission(cur_user|->file) or #gg.(gg: in_group[{cur_user}] & gg|->file: dom(permission) & RR: permission(gg|->file))));
  List_Precondition(Machine(DAC),dac_write)==(file: files & file: wopened & data: seq(BYTE) & (cur_user|->file: dom(permission) & WW: permission(cur_user|->file) or #gg.(gg: in_group[{cur_user}] & gg|->file: dom(permission) & WW: permission(gg|->file))))
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(DAC),dac_write)==(file: files & file: wopened & data: seq(BYTE) & (cur_user|->file: dom(permission) & WW: permission(cur_user|->file) or #gg.(gg: in_group[{cur_user}] & gg|->file: dom(permission) & WW: permission(gg|->file))) & file: files & file: wopened & data: seq(BYTE) | contents:=contents<+{file|->(contents(file)^data)});
  Expanded_List_Substitution(Machine(DAC),dac_read)==(file: files & file: ropened & contents(file)/=<> & (cur_user|->file: dom(permission) & RR: permission(cur_user|->file) or #gg.(gg: in_group[{cur_user}] & gg|->file: dom(permission) & RR: permission(gg|->file))) & file: files & file: ropened & contents(file)/=<> | position(file): dom(contents(file)) ==> byte,position:=contents(file)(position(file)),position<+{file|->position(file)+1} [] not(position(file): dom(contents(file))) ==> byte:=eof);
  Expanded_List_Substitution(Machine(DAC),dac_close)==(file: files & file: ropened\/wopened | closed,ropened,wopened,position:=closed\/{file},ropened-{file},wopened-{file},{file}<<|position);
  Expanded_List_Substitution(Machine(DAC),dac_wopen)==(file: files & file: closed & (cur_user|->file: dom(permission) & WW: permission(cur_user|->file) or #gg.(gg: in_group[{cur_user}] & gg|->file: dom(permission) & WW: permission(gg|->file))) & file: files & file: closed | closed,wopened:=closed-{file},wopened\/{file});
  Expanded_List_Substitution(Machine(DAC),dac_ropen)==(file: files & file: closed & (cur_user|->file: dom(permission) & RR: permission(cur_user|->file) or #gg.(gg: in_group[{cur_user}] & gg|->file: dom(permission) & RR: permission(gg|->file))) & file: files & file: closed | closed,ropened,position:=closed-{file},ropened\/{file},position<+{file|->1});
  Expanded_List_Substitution(Machine(DAC),dac_delete)==(obj: files\/directories & tcl(objectTree~)[{obj}]\/{obj}/\files <: closed & owner(obj) = cur_user & obj: files\/directories & tcl(objectTree~)[{obj}]\/{obj}/\files <: closed | @to_delete.(to_delete = tcl(objectTree~)[{obj}]\/{obj} ==> files,closed,directories,objectTree,contents:=files-to_delete,closed-to_delete,directories-to_delete,to_delete<<|objectTree|>>to_delete,to_delete<<|contents) || permission:=(user\/group)*{obj}<<|permission);
  Expanded_List_Substitution(Machine(DAC),dac_copy)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & (cur_user|->destDir: dom(permission) & WW: permission(cur_user|->destDir) or #gg.(gg: in_group[{cur_user}] & gg|->destDir: dom(permission) & WW: permission(gg|->destDir))) & (cur_user|->obj: dom(permission) & RR: permission(cur_user|->obj) or #gg.(gg: in_group[{cur_user}] & gg|->obj: dom(permission) & RR: permission(gg|->obj))) & obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) | @(cp$0).(cp$0: OBJECT & cp$0/:(files\/directories) ==> (obj: files ==> files,objectTree,contents,closed:=files\/{cp$0},objectTree<+{cp$0|->destDir},contents<+{cp$0|->contents(obj)},closed\/{cp$0} [] not(obj: files) ==> directories,objectTree:=directories\/{cp$0},objectTree<+{cp$0|->destDir} || cp:=cp$0)) || owner:=owner<+{cp|->cur_user} || permission:=permission\/{cur_user|->cp|->{RR,WW},su|->cp|->{RR,WW}});
  Expanded_List_Substitution(Machine(DAC),dac_move)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & destDir/=obj & (cur_user|->destDir: dom(permission) & WW: permission(cur_user|->destDir) or #gg.(gg: in_group[{cur_user}] & gg|->destDir: dom(permission) & WW: permission(gg|->destDir))) & (cur_user|->obj: dom(permission) & RR: permission(cur_user|->obj) or #gg.(gg: in_group[{cur_user}] & gg|->obj: dom(permission) & RR: permission(gg|->obj))) & obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & destDir/=obj | objectTree:=objectTree<+{obj|->destDir});
  Expanded_List_Substitution(Machine(DAC),dac_create_dir)==(dir: OBJECT & dir/:files & dir/:directories & parent_dir: directories & (cur_user|->parent_dir: dom(permission) & WW: permission(cur_user|->parent_dir) or #gg.(gg: in_group[{cur_user}] & gg|->parent_dir: dom(permission) & WW: permission(gg|->parent_dir))) & dir: OBJECT & dir/:files & dir/:directories & parent_dir: directories | directories,objectTree:=directories\/{dir},objectTree<+{dir|->parent_dir} || owner:=owner<+{dir|->cur_user} || permission:=permission\/{cur_user|->dir|->{RR,WW},su|->dir|->{RR,WW}});
  Expanded_List_Substitution(Machine(DAC),dac_create_file)==(file: OBJECT & file/:files & file/:directories & parent_dir: directories & (cur_user|->parent_dir: dom(permission) & WW: permission(cur_user|->parent_dir) or #gg.(gg: in_group[{cur_user}] & gg|->parent_dir: dom(permission) & WW: permission(gg|->parent_dir))) & file: OBJECT & file/:files & file/:directories & parent_dir: directories | files,objectTree,closed,contents:=files\/{file},objectTree<+{file|->parent_dir},closed\/{file},contents<+{file|-><>} || owner:=owner<+{file|->cur_user} || permission:=permission\/{cur_user|->file|->{RR,WW},su|->file|->{RR,WW}});
  Expanded_List_Substitution(Machine(DAC),revoke_permission)==(cur_user = su & ug: user\/group & obj: directories\/files & pp: RIGHT & ug|->obj: dom(permission) & pp: permission(ug|->obj) & ug/=su & owner(obj)/=ug & ug/=null | permission:=permission<+{ug|->obj|->permission(ug|->obj)-{pp}});
  Expanded_List_Substitution(Machine(DAC),grant_permission)==(cur_user = su & ug: user\/group & obj: directories\/files & pp: RIGHT & ug/=null | ug|->obj: dom(permission) ==> permission:=permission<+{ug|->obj|->({pp}\/permission(ug|->obj))} [] not(ug|->obj: dom(permission)) ==> permission:=permission<+{ug|->obj|->{pp}});
  Expanded_List_Substitution(Machine(DAC),remove_group)==(cur_user = su & gg: group & uu: user & gg: in_group[{uu}] & uu/=null | in_group:=in_group-{uu|->gg});
  Expanded_List_Substitution(Machine(DAC),add_group)==(cur_user = su & gg: group & uu: user & gg/:in_group[{uu}] & uu/=null | in_group:=in_group\/{uu|->gg});
  Expanded_List_Substitution(Machine(DAC),delete_group)==(cur_user = su & gg: group | group,permission,in_group:=group-{gg},{gg}*(directories\/files)<<|permission,in_group|>>{gg});
  Expanded_List_Substitution(Machine(DAC),create_group)==(cur_user = su & gg: UG & gg/:group & gg/:user | group:=group\/{gg});
  Expanded_List_Substitution(Machine(DAC),delete_user)==(cur_user = su & uu: user & uu/=su & uu/=null | user,permission,in_group:=user-{uu},{uu}*(directories\/files)<<|permission,{uu}<<|in_group || @rr.(rr: directories\/files +-> {su} & dom(rr) = dom(owner|>{uu}) ==> owner:=owner<+rr));
  Expanded_List_Substitution(Machine(DAC),create_user)==(cur_user = su & uu: UG & uu/:user & uu/:group | user:=user\/{uu});
  Expanded_List_Substitution(Machine(DAC),logout)==(cur_user/=null | cur_user:=null);
  Expanded_List_Substitution(Machine(DAC),login)==(uu: user & cur_user = null | cur_user:=uu);
  List_Substitution(Machine(DAC),login)==(cur_user:=uu);
  List_Substitution(Machine(DAC),logout)==(cur_user:=null);
  List_Substitution(Machine(DAC),create_user)==(user:=user\/{uu});
  List_Substitution(Machine(DAC),delete_user)==(user:=user-{uu} || permission:={uu}*(directories\/files)<<|permission || in_group:={uu}<<|in_group || ANY rr WHERE rr: directories\/files +-> {su} & dom(rr) = dom(owner|>{uu}) THEN owner:=owner<+rr END);
  List_Substitution(Machine(DAC),create_group)==(group:=group\/{gg});
  List_Substitution(Machine(DAC),delete_group)==(group:=group-{gg} || permission:={gg}*(directories\/files)<<|permission || in_group:=in_group|>>{gg});
  List_Substitution(Machine(DAC),add_group)==(in_group:=in_group\/{uu|->gg});
  List_Substitution(Machine(DAC),remove_group)==(in_group:=in_group-{uu|->gg});
  List_Substitution(Machine(DAC),grant_permission)==(IF ug|->obj: dom(permission) THEN permission(ug|->obj):={pp}\/permission(ug|->obj) ELSE permission(ug|->obj):={pp} END);
  List_Substitution(Machine(DAC),revoke_permission)==(permission(ug|->obj):=permission(ug|->obj)-{pp});
  List_Substitution(Machine(DAC),dac_create_file)==(create_file(file,parent_dir) || owner(file):=cur_user || permission:=permission\/{cur_user|->file|->{RR,WW},su|->file|->{RR,WW}});
  List_Substitution(Machine(DAC),dac_create_dir)==(create_dir(dir,parent_dir) || owner(dir):=cur_user || permission:=permission\/{cur_user|->dir|->{RR,WW},su|->dir|->{RR,WW}});
  List_Substitution(Machine(DAC),dac_move)==(move(obj,destDir));
  List_Substitution(Machine(DAC),dac_copy)==(cp <-- copy(obj,destDir) || owner(cp):=cur_user || permission:=permission\/{cur_user|->cp|->{RR,WW},su|->cp|->{RR,WW}});
  List_Substitution(Machine(DAC),dac_delete)==(delete(obj) || permission:=(user\/group)*{obj}<<|permission);
  List_Substitution(Machine(DAC),dac_ropen)==(ropen(file));
  List_Substitution(Machine(DAC),dac_wopen)==(wopen(file));
  List_Substitution(Machine(DAC),dac_close)==(close(file));
  List_Substitution(Machine(DAC),dac_read)==(byte <-- read(file));
  List_Substitution(Machine(DAC),dac_write)==(write(file,data))
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(DAC))==(root,tcl,eof,su,null);
  Inherited_List_Constants(Machine(DAC))==(root,tcl,eof);
  List_Constants(Machine(DAC))==(su,null)
END
&
THEORY ListSetsX IS
  Set_Definition(Machine(DAC),BYTE)==(?);
  Context_List_Enumerated(Machine(DAC))==(?);
  Context_List_Defered(Machine(DAC))==(?);
  Context_List_Sets(Machine(DAC))==(?);
  List_Valuable_Sets(Machine(DAC))==(OBJECT,BYTE,UG);
  Inherited_List_Enumerated(Machine(DAC))==(?);
  Inherited_List_Defered(Machine(DAC))==(OBJECT,BYTE);
  Inherited_List_Sets(Machine(DAC))==(OBJECT,BYTE);
  List_Enumerated(Machine(DAC))==(RIGHT);
  List_Defered(Machine(DAC))==(UG);
  List_Sets(Machine(DAC))==(UG,RIGHT);
  Set_Definition(Machine(DAC),OBJECT)==(?);
  Set_Definition(Machine(DAC),UG)==(?);
  Set_Definition(Machine(DAC),RIGHT)==({RR,WW})
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(DAC))==(?);
  Expanded_List_HiddenConstants(Machine(DAC))==(?);
  List_HiddenConstants(Machine(DAC))==(?);
  External_List_HiddenConstants(Machine(DAC))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(DAC))==(btrue);
  Context_List_Properties(Machine(DAC))==(btrue);
  Inherited_List_Properties(Machine(DAC))==(root: OBJECT & eof: BYTE & tcl: OBJECT <-> OBJECT --> (OBJECT <-> OBJECT) & !rr.(rr: OBJECT <-> OBJECT => rr <: tcl(rr)) & !rr.(rr: OBJECT <-> OBJECT => (rr;tcl(rr)) <: tcl(rr)) & OBJECT: FIN(INTEGER) & not(OBJECT = {}) & BYTE: FIN(INTEGER) & not(BYTE = {}));
  List_Properties(Machine(DAC))==(su: UG & null: UG & su/=null & UG: FIN(INTEGER) & not(UG = {}) & RIGHT: FIN(INTEGER) & not(RIGHT = {}))
END
&
THEORY ListSeenInfoX END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(DAC),login)==(?);
  List_ANY_Var(Machine(DAC),logout)==(?);
  List_ANY_Var(Machine(DAC),create_user)==(?);
  List_ANY_Var(Machine(DAC),delete_user)==(Var(rr) == SetOf(atype(OBJECT,?,?)*atype(UG,?,?)));
  List_ANY_Var(Machine(DAC),create_group)==(?);
  List_ANY_Var(Machine(DAC),delete_group)==(?);
  List_ANY_Var(Machine(DAC),add_group)==(?);
  List_ANY_Var(Machine(DAC),remove_group)==(?);
  List_ANY_Var(Machine(DAC),grant_permission)==(?);
  List_ANY_Var(Machine(DAC),revoke_permission)==(?);
  List_ANY_Var(Machine(DAC),dac_create_file)==(?);
  List_ANY_Var(Machine(DAC),dac_create_dir)==(?);
  List_ANY_Var(Machine(DAC),dac_move)==(?);
  List_ANY_Var(Machine(DAC),dac_copy)==(?);
  List_ANY_Var(Machine(DAC),dac_delete)==(?);
  List_ANY_Var(Machine(DAC),dac_ropen)==(?);
  List_ANY_Var(Machine(DAC),dac_wopen)==(?);
  List_ANY_Var(Machine(DAC),dac_close)==(?);
  List_ANY_Var(Machine(DAC),dac_read)==(?);
  List_ANY_Var(Machine(DAC),dac_write)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(DAC)) == (su,null,UG,RIGHT,RR,WW | root,tcl,eof,OBJECT,BYTE | cur_user,in_group,permission,group,user,owner | position,wopened,ropened,closed,contents,directories,files,objectTree | login,logout,create_user,delete_user,create_group,delete_group,add_group,remove_group,grant_permission,revoke_permission,dac_create_file,dac_create_dir,dac_move,dac_copy,dac_delete,dac_ropen,dac_wopen,dac_close,dac_read,dac_write | ? | included(Machine(FileSystem)) | ? | DAC);
  List_Of_HiddenCst_Ids(Machine(DAC)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(DAC)) == (su,null,root,tcl,eof);
  List_Of_VisibleVar_Ids(Machine(DAC)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(DAC)) == (?: ?);
  List_Of_Ids(Machine(FileSystem)) == (root,tcl,eof,OBJECT,BYTE | ? | position,wopened,ropened,closed,contents,directories,files,objectTree | ? | create_file,create_dir,move,copy,delete,ropen,wopen,close,read,write | ? | ? | ? | FileSystem);
  List_Of_HiddenCst_Ids(Machine(FileSystem)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(FileSystem)) == (root,tcl,eof);
  List_Of_VisibleVar_Ids(Machine(FileSystem)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(FileSystem)) == (?: ?)
END
&
THEORY SetsEnvX IS
  Sets(Machine(DAC)) == (Type(BYTE) == Cst(SetOf(atype(BYTE,"[BYTE","]BYTE")));Type(OBJECT) == Cst(SetOf(atype(OBJECT,"[OBJECT","]OBJECT")));Type(UG) == Cst(SetOf(atype(UG,"[UG","]UG")));Type(RIGHT) == Cst(SetOf(etype(RIGHT,0,1))))
END
&
THEORY ConstantsEnvX IS
  Constants(Machine(DAC)) == (Type(eof) == Cst(atype(BYTE,?,?));Type(tcl) == Cst(SetOf(SetOf(atype(OBJECT,?,?)*atype(OBJECT,?,?))*SetOf(atype(OBJECT,?,?)*atype(OBJECT,?,?))));Type(root) == Cst(atype(OBJECT,?,?));Type(RR) == Cst(etype(RIGHT,0,1));Type(WW) == Cst(etype(RIGHT,0,1));Type(su) == Cst(atype(UG,?,?));Type(null) == Cst(atype(UG,?,?)))
END
&
THEORY VariablesEnvX IS
  Variables(Machine(DAC)) == (Type(objectTree) == Mvl(SetOf(atype(OBJECT,?,?)*atype(OBJECT,?,?)));Type(files) == Mvl(SetOf(atype(OBJECT,?,?)));Type(directories) == Mvl(SetOf(atype(OBJECT,?,?)));Type(contents) == Mvl(SetOf(atype(OBJECT,?,?)*SetOf(btype(INTEGER,?,?)*atype(BYTE,?,?))));Type(closed) == Mvl(SetOf(atype(OBJECT,?,?)));Type(ropened) == Mvl(SetOf(atype(OBJECT,?,?)));Type(wopened) == Mvl(SetOf(atype(OBJECT,?,?)));Type(position) == Mvl(SetOf(atype(OBJECT,?,?)*btype(INTEGER,1,MAXINT)));Type(cur_user) == Mvl(atype(UG,?,?));Type(in_group) == Mvl(SetOf(atype(UG,?,?)*atype(UG,?,?)));Type(permission) == Mvl(SetOf(atype(UG,?,?)*atype(OBJECT,?,?)*SetOf(etype(RIGHT,?,?))));Type(group) == Mvl(SetOf(atype(UG,?,?)));Type(user) == Mvl(SetOf(atype(UG,?,?)));Type(owner) == Mvl(SetOf(atype(OBJECT,?,?)*atype(UG,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(DAC)) == (Type(dac_write) == Cst(No_type,atype(OBJECT,?,?)*SetOf(btype(INTEGER,?,?)*atype(BYTE,?,?)));Type(dac_read) == Cst(atype(BYTE,?,?),atype(OBJECT,?,?));Type(dac_close) == Cst(No_type,atype(OBJECT,?,?));Type(dac_wopen) == Cst(No_type,atype(OBJECT,?,?));Type(dac_ropen) == Cst(No_type,atype(OBJECT,?,?));Type(dac_delete) == Cst(No_type,atype(OBJECT,?,?));Type(dac_copy) == Cst(atype(OBJECT,?,?),atype(OBJECT,?,?)*atype(OBJECT,?,?));Type(dac_move) == Cst(No_type,atype(OBJECT,?,?)*atype(OBJECT,?,?));Type(dac_create_dir) == Cst(No_type,atype(OBJECT,?,?)*atype(OBJECT,?,?));Type(dac_create_file) == Cst(No_type,atype(OBJECT,?,?)*atype(OBJECT,?,?));Type(revoke_permission) == Cst(No_type,atype(UG,?,?)*atype(OBJECT,?,?)*etype(RIGHT,?,?));Type(grant_permission) == Cst(No_type,atype(UG,?,?)*atype(OBJECT,?,?)*etype(RIGHT,?,?));Type(remove_group) == Cst(No_type,atype(UG,?,?)*atype(UG,?,?));Type(add_group) == Cst(No_type,atype(UG,?,?)*atype(UG,?,?));Type(delete_group) == Cst(No_type,atype(UG,?,?));Type(create_group) == Cst(No_type,atype(UG,?,?));Type(delete_user) == Cst(No_type,atype(UG,?,?));Type(create_user) == Cst(No_type,atype(UG,?,?));Type(logout) == Cst(No_type,No_type);Type(login) == Cst(No_type,atype(UG,?,?)));
  Observers(Machine(DAC)) == (Type(dac_read) == Cst(atype(BYTE,?,?),atype(OBJECT,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
