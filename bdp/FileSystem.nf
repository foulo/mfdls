﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Machine(FileSystem))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Machine(FileSystem))==(Machine(FileSystem));
  Level(Machine(FileSystem))==(0)
END
&
THEORY LoadedStructureX IS
  Machine(FileSystem)
END
&
THEORY ListSeesX IS
  List_Sees(Machine(FileSystem))==(?)
END
&
THEORY ListUsesX IS
  List_Uses(Machine(FileSystem))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Machine(FileSystem))==(?);
  List_Includes(Machine(FileSystem))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Machine(FileSystem))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Machine(FileSystem))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Machine(FileSystem))==(?);
  Context_List_Variables(Machine(FileSystem))==(?);
  Abstract_List_Variables(Machine(FileSystem))==(?);
  Local_List_Variables(Machine(FileSystem))==(position,wopened,ropened,closed,contents,directories,files,objectTree);
  List_Variables(Machine(FileSystem))==(position,wopened,ropened,closed,contents,directories,files,objectTree);
  External_List_Variables(Machine(FileSystem))==(position,wopened,ropened,closed,contents,directories,files,objectTree)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Machine(FileSystem))==(?);
  Abstract_List_VisibleVariables(Machine(FileSystem))==(?);
  External_List_VisibleVariables(Machine(FileSystem))==(?);
  Expanded_List_VisibleVariables(Machine(FileSystem))==(?);
  List_VisibleVariables(Machine(FileSystem))==(?);
  Internal_List_VisibleVariables(Machine(FileSystem))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Machine(FileSystem))==(btrue);
  Gluing_List_Invariant(Machine(FileSystem))==(btrue);
  Expanded_List_Invariant(Machine(FileSystem))==(btrue);
  Abstract_List_Invariant(Machine(FileSystem))==(btrue);
  Context_List_Invariant(Machine(FileSystem))==(btrue);
  List_Invariant(Machine(FileSystem))==(files <: OBJECT & directories <: OBJECT & objectTree: directories\/files --> directories & files/\directories = {} & contents: files --> seq(BYTE) & closed <: files & ropened <: files & wopened <: files & closed/\ropened = {} & closed/\wopened = {} & wopened/\ropened = {} & closed\/ropened\/wopened = files & position: ropened --> NAT1)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Machine(FileSystem))==(btrue);
  Abstract_List_Assertions(Machine(FileSystem))==(btrue);
  Context_List_Assertions(Machine(FileSystem))==(btrue);
  List_Assertions(Machine(FileSystem))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Machine(FileSystem))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Machine(FileSystem))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Machine(FileSystem))==(files,directories,objectTree,contents,closed,ropened,wopened,position:={},{root},{root|->root},{},{},{},{},{});
  Context_List_Initialisation(Machine(FileSystem))==(skip);
  List_Initialisation(Machine(FileSystem))==(files,directories,objectTree,contents,closed,ropened,wopened,position:={},{root},{root|->root},{},{},{},{},{})
END
&
THEORY ListParametersX IS
  List_Parameters(Machine(FileSystem))==(?)
END
&
THEORY ListInstanciatedParametersX END
&
THEORY ListConstraintsX IS
  List_Context_Constraints(Machine(FileSystem))==(btrue);
  List_Constraints(Machine(FileSystem))==(btrue)
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Machine(FileSystem))==(create_file,create_dir,move,copy,delete,ropen,wopen,close,read,write);
  List_Operations(Machine(FileSystem))==(create_file,create_dir,move,copy,delete,ropen,wopen,close,read,write)
END
&
THEORY ListInputX IS
  List_Input(Machine(FileSystem),create_file)==(file,parent_dir);
  List_Input(Machine(FileSystem),create_dir)==(dir,parent_dir);
  List_Input(Machine(FileSystem),move)==(obj,destDir);
  List_Input(Machine(FileSystem),copy)==(obj,destDir);
  List_Input(Machine(FileSystem),delete)==(obj);
  List_Input(Machine(FileSystem),ropen)==(file);
  List_Input(Machine(FileSystem),wopen)==(file);
  List_Input(Machine(FileSystem),close)==(file);
  List_Input(Machine(FileSystem),read)==(file);
  List_Input(Machine(FileSystem),write)==(file,data)
END
&
THEORY ListOutputX IS
  List_Output(Machine(FileSystem),create_file)==(?);
  List_Output(Machine(FileSystem),create_dir)==(?);
  List_Output(Machine(FileSystem),move)==(?);
  List_Output(Machine(FileSystem),copy)==(obj2);
  List_Output(Machine(FileSystem),delete)==(?);
  List_Output(Machine(FileSystem),ropen)==(?);
  List_Output(Machine(FileSystem),wopen)==(?);
  List_Output(Machine(FileSystem),close)==(?);
  List_Output(Machine(FileSystem),read)==(byte);
  List_Output(Machine(FileSystem),write)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Machine(FileSystem),create_file)==(create_file(file,parent_dir));
  List_Header(Machine(FileSystem),create_dir)==(create_dir(dir,parent_dir));
  List_Header(Machine(FileSystem),move)==(move(obj,destDir));
  List_Header(Machine(FileSystem),copy)==(obj2 <-- copy(obj,destDir));
  List_Header(Machine(FileSystem),delete)==(delete(obj));
  List_Header(Machine(FileSystem),ropen)==(ropen(file));
  List_Header(Machine(FileSystem),wopen)==(wopen(file));
  List_Header(Machine(FileSystem),close)==(close(file));
  List_Header(Machine(FileSystem),read)==(byte <-- read(file));
  List_Header(Machine(FileSystem),write)==(write(file,data))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  List_Precondition(Machine(FileSystem),create_file)==(file: OBJECT & file/:files & file/:directories & parent_dir: directories);
  List_Precondition(Machine(FileSystem),create_dir)==(dir: OBJECT & dir/:files & dir/:directories & parent_dir: directories);
  List_Precondition(Machine(FileSystem),move)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & destDir/=obj);
  List_Precondition(Machine(FileSystem),copy)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree));
  List_Precondition(Machine(FileSystem),delete)==(obj: files\/directories & tcl(objectTree~)[{obj}]\/{obj}/\files <: closed);
  List_Precondition(Machine(FileSystem),ropen)==(file: files & file: closed);
  List_Precondition(Machine(FileSystem),wopen)==(file: files & file: closed);
  List_Precondition(Machine(FileSystem),close)==(file: files & file: ropened\/wopened);
  List_Precondition(Machine(FileSystem),read)==(file: files & file: ropened & contents(file)/=<>);
  List_Precondition(Machine(FileSystem),write)==(file: files & file: wopened & data: seq(BYTE))
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Machine(FileSystem),write)==(file: files & file: wopened & data: seq(BYTE) | contents:=contents<+{file|->(contents(file)^data)});
  Expanded_List_Substitution(Machine(FileSystem),read)==(file: files & file: ropened & contents(file)/=<> | position(file): dom(contents(file)) ==> byte,position:=contents(file)(position(file)),position<+{file|->position(file)+1} [] not(position(file): dom(contents(file))) ==> byte:=eof);
  Expanded_List_Substitution(Machine(FileSystem),close)==(file: files & file: ropened\/wopened | closed,ropened,wopened,position:=closed\/{file},ropened-{file},wopened-{file},{file}<<|position);
  Expanded_List_Substitution(Machine(FileSystem),wopen)==(file: files & file: closed | closed,wopened:=closed-{file},wopened\/{file});
  Expanded_List_Substitution(Machine(FileSystem),ropen)==(file: files & file: closed | closed,ropened,position:=closed-{file},ropened\/{file},position<+{file|->1});
  Expanded_List_Substitution(Machine(FileSystem),delete)==(obj: files\/directories & tcl(objectTree~)[{obj}]\/{obj}/\files <: closed | @to_delete.(to_delete = tcl(objectTree~)[{obj}]\/{obj} ==> files,closed,directories,objectTree,contents:=files-to_delete,closed-to_delete,directories-to_delete,to_delete<<|objectTree|>>to_delete,to_delete<<|contents));
  Expanded_List_Substitution(Machine(FileSystem),copy)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) | @cp.(cp: OBJECT & cp/:(files\/directories) ==> (obj: files ==> files,objectTree,contents,closed:=files\/{cp},objectTree<+{cp|->destDir},contents<+{cp|->contents(obj)},closed\/{cp} [] not(obj: files) ==> directories,objectTree:=directories\/{cp},objectTree<+{cp|->destDir} || obj2:=cp)));
  Expanded_List_Substitution(Machine(FileSystem),move)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & destDir/=obj | objectTree:=objectTree<+{obj|->destDir});
  Expanded_List_Substitution(Machine(FileSystem),create_dir)==(dir: OBJECT & dir/:files & dir/:directories & parent_dir: directories | directories,objectTree:=directories\/{dir},objectTree<+{dir|->parent_dir});
  Expanded_List_Substitution(Machine(FileSystem),create_file)==(file: OBJECT & file/:files & file/:directories & parent_dir: directories | files,objectTree,closed,contents:=files\/{file},objectTree<+{file|->parent_dir},closed\/{file},contents<+{file|-><>});
  List_Substitution(Machine(FileSystem),create_file)==(files:=files\/{file} || objectTree:=objectTree<+{file|->parent_dir} || closed:=closed\/{file} || contents(file):=<>);
  List_Substitution(Machine(FileSystem),create_dir)==(directories:=directories\/{dir} || objectTree:=objectTree<+{dir|->parent_dir});
  List_Substitution(Machine(FileSystem),move)==(objectTree:=objectTree<+{obj|->destDir});
  List_Substitution(Machine(FileSystem),copy)==(ANY cp WHERE cp: OBJECT & cp/:(files\/directories) THEN IF obj: files THEN files:=files\/{cp} || objectTree(cp):=destDir || contents(cp):=contents(obj) || closed:=closed\/{cp} ELSE directories:=directories\/{cp} || objectTree(cp):=destDir END || obj2:=cp END);
  List_Substitution(Machine(FileSystem),delete)==(LET to_delete BE to_delete = tcl(objectTree~)[{obj}]\/{obj} IN files:=files-to_delete || closed:=closed-to_delete || directories:=directories-to_delete || objectTree:=to_delete<<|objectTree|>>to_delete || contents:=to_delete<<|contents END);
  List_Substitution(Machine(FileSystem),ropen)==(closed:=closed-{file} || ropened:=ropened\/{file} || position(file):=1);
  List_Substitution(Machine(FileSystem),wopen)==(closed:=closed-{file} || wopened:=wopened\/{file});
  List_Substitution(Machine(FileSystem),close)==(closed:=closed\/{file} || ropened:=ropened-{file} || wopened:=wopened-{file} || position:={file}<<|position);
  List_Substitution(Machine(FileSystem),read)==(IF position(file): dom(contents(file)) THEN byte:=contents(file)(position(file)) || position(file):=position(file)+1 ELSE byte:=eof END);
  List_Substitution(Machine(FileSystem),write)==(contents(file):=contents(file)^data)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Machine(FileSystem))==(root,tcl,eof);
  Inherited_List_Constants(Machine(FileSystem))==(?);
  List_Constants(Machine(FileSystem))==(root,tcl,eof)
END
&
THEORY ListSetsX IS
  Set_Definition(Machine(FileSystem),OBJECT)==(?);
  Context_List_Enumerated(Machine(FileSystem))==(?);
  Context_List_Defered(Machine(FileSystem))==(?);
  Context_List_Sets(Machine(FileSystem))==(?);
  List_Valuable_Sets(Machine(FileSystem))==(OBJECT,BYTE);
  Inherited_List_Enumerated(Machine(FileSystem))==(?);
  Inherited_List_Defered(Machine(FileSystem))==(?);
  Inherited_List_Sets(Machine(FileSystem))==(?);
  List_Enumerated(Machine(FileSystem))==(?);
  List_Defered(Machine(FileSystem))==(OBJECT,BYTE);
  List_Sets(Machine(FileSystem))==(OBJECT,BYTE);
  Set_Definition(Machine(FileSystem),BYTE)==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Machine(FileSystem))==(?);
  Expanded_List_HiddenConstants(Machine(FileSystem))==(?);
  List_HiddenConstants(Machine(FileSystem))==(?);
  External_List_HiddenConstants(Machine(FileSystem))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Machine(FileSystem))==(btrue);
  Context_List_Properties(Machine(FileSystem))==(btrue);
  Inherited_List_Properties(Machine(FileSystem))==(btrue);
  List_Properties(Machine(FileSystem))==(root: OBJECT & eof: BYTE & tcl: OBJECT <-> OBJECT --> (OBJECT <-> OBJECT) & !rr.(rr: OBJECT <-> OBJECT => rr <: tcl(rr)) & !rr.(rr: OBJECT <-> OBJECT => (rr;tcl(rr)) <: tcl(rr)) & OBJECT: FIN(INTEGER) & not(OBJECT = {}) & BYTE: FIN(INTEGER) & not(BYTE = {}))
END
&
THEORY ListSeenInfoX END
&
THEORY ListANYVarX IS
  List_ANY_Var(Machine(FileSystem),create_file)==(?);
  List_ANY_Var(Machine(FileSystem),create_dir)==(?);
  List_ANY_Var(Machine(FileSystem),move)==(?);
  List_ANY_Var(Machine(FileSystem),copy)==(Var(cp) == atype(OBJECT,?,?));
  List_ANY_Var(Machine(FileSystem),delete)==(?);
  List_ANY_Var(Machine(FileSystem),ropen)==(?);
  List_ANY_Var(Machine(FileSystem),wopen)==(?);
  List_ANY_Var(Machine(FileSystem),close)==(?);
  List_ANY_Var(Machine(FileSystem),read)==(?);
  List_ANY_Var(Machine(FileSystem),write)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Machine(FileSystem)) == (root,tcl,eof,OBJECT,BYTE | ? | position,wopened,ropened,closed,contents,directories,files,objectTree | ? | create_file,create_dir,move,copy,delete,ropen,wopen,close,read,write | ? | ? | ? | FileSystem);
  List_Of_HiddenCst_Ids(Machine(FileSystem)) == (? | ?);
  List_Of_VisibleCst_Ids(Machine(FileSystem)) == (root,tcl,eof);
  List_Of_VisibleVar_Ids(Machine(FileSystem)) == (? | ?);
  List_Of_Ids_SeenBNU(Machine(FileSystem)) == (?: ?)
END
&
THEORY SetsEnvX IS
  Sets(Machine(FileSystem)) == (Type(OBJECT) == Cst(SetOf(atype(OBJECT,"[OBJECT","]OBJECT")));Type(BYTE) == Cst(SetOf(atype(BYTE,"[BYTE","]BYTE"))))
END
&
THEORY ConstantsEnvX IS
  Constants(Machine(FileSystem)) == (Type(root) == Cst(atype(OBJECT,?,?));Type(tcl) == Cst(SetOf(SetOf(atype(OBJECT,?,?)*atype(OBJECT,?,?))*SetOf(atype(OBJECT,?,?)*atype(OBJECT,?,?))));Type(eof) == Cst(atype(BYTE,?,?)))
END
&
THEORY VariablesEnvX IS
  Variables(Machine(FileSystem)) == (Type(position) == Mvl(SetOf(atype(OBJECT,?,?)*btype(INTEGER,1,MAXINT)));Type(wopened) == Mvl(SetOf(atype(OBJECT,?,?)));Type(ropened) == Mvl(SetOf(atype(OBJECT,?,?)));Type(closed) == Mvl(SetOf(atype(OBJECT,?,?)));Type(contents) == Mvl(SetOf(atype(OBJECT,?,?)*SetOf(btype(INTEGER,?,?)*atype(BYTE,?,?))));Type(directories) == Mvl(SetOf(atype(OBJECT,?,?)));Type(files) == Mvl(SetOf(atype(OBJECT,?,?)));Type(objectTree) == Mvl(SetOf(atype(OBJECT,?,?)*atype(OBJECT,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Machine(FileSystem)) == (Type(write) == Cst(No_type,atype(OBJECT,?,?)*SetOf(btype(INTEGER,?,?)*atype(BYTE,?,?)));Type(read) == Cst(atype(BYTE,?,?),atype(OBJECT,?,?));Type(close) == Cst(No_type,atype(OBJECT,?,?));Type(wopen) == Cst(No_type,atype(OBJECT,?,?));Type(ropen) == Cst(No_type,atype(OBJECT,?,?));Type(delete) == Cst(No_type,atype(OBJECT,?,?));Type(copy) == Cst(atype(OBJECT,?,?),atype(OBJECT,?,?)*atype(OBJECT,?,?));Type(move) == Cst(No_type,atype(OBJECT,?,?)*atype(OBJECT,?,?));Type(create_dir) == Cst(No_type,atype(OBJECT,?,?)*atype(OBJECT,?,?));Type(create_file) == Cst(No_type,atype(OBJECT,?,?)*atype(OBJECT,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
