﻿Normalised(
THEORY MagicNumberX IS
  MagicNumber(Refinement(FileSystem_r))==(3.5)
END
&
THEORY UpperLevelX IS
  First_Level(Refinement(FileSystem_r))==(Machine(FileSystem));
  Level(Refinement(FileSystem_r))==(1);
  Upper_Level(Refinement(FileSystem_r))==(Machine(FileSystem))
END
&
THEORY LoadedStructureX IS
  Refinement(FileSystem_r)
END
&
THEORY ListSeesX IS
  List_Sees(Refinement(FileSystem_r))==(?)
END
&
THEORY ListIncludesX IS
  Inherited_List_Includes(Refinement(FileSystem_r))==(?);
  List_Includes(Refinement(FileSystem_r))==(?)
END
&
THEORY ListPromotesX IS
  List_Promotes(Refinement(FileSystem_r))==(?)
END
&
THEORY ListExtendsX IS
  List_Extends(Refinement(FileSystem_r))==(?)
END
&
THEORY ListVariablesX IS
  External_Context_List_Variables(Refinement(FileSystem_r))==(?);
  Context_List_Variables(Refinement(FileSystem_r))==(?);
  Abstract_List_Variables(Refinement(FileSystem_r))==(position,wopened,ropened,closed,contents,directories,files,objectTree);
  Local_List_Variables(Refinement(FileSystem_r))==(?);
  List_Variables(Refinement(FileSystem_r))==(?);
  External_List_Variables(Refinement(FileSystem_r))==(?)
END
&
THEORY ListVisibleVariablesX IS
  Inherited_List_VisibleVariables(Refinement(FileSystem_r))==(?);
  Abstract_List_VisibleVariables(Refinement(FileSystem_r))==(?);
  External_List_VisibleVariables(Refinement(FileSystem_r))==(?);
  Expanded_List_VisibleVariables(Refinement(FileSystem_r))==(?);
  List_VisibleVariables(Refinement(FileSystem_r))==(position,wopened,ropened,closed,contents,directories,files,objectTree);
  Internal_List_VisibleVariables(Refinement(FileSystem_r))==(position,wopened,ropened,closed,contents,directories,files,objectTree)
END
&
THEORY ListOfNewVariablesX IS
  List_Of_New_Variables(Refinement(FileSystem_r))==(?)
END
&
THEORY ListInvariantX IS
  Gluing_Seen_List_Invariant(Refinement(FileSystem_r))==(btrue);
  Expanded_List_Invariant(Refinement(FileSystem_r))==(btrue);
  Abstract_List_Invariant(Refinement(FileSystem_r))==(files <: OBJECT & directories <: OBJECT & objectTree: directories\/files --> directories & files/\directories = {} & contents: files --> seq(BYTE) & closed <: files & ropened <: files & wopened <: files & closed/\ropened = {} & closed/\wopened = {} & wopened/\ropened = {} & closed\/ropened\/wopened = files & position: ropened --> NAT1);
  Context_List_Invariant(Refinement(FileSystem_r))==(btrue);
  List_Invariant(Refinement(FileSystem_r))==(btrue)
END
&
THEORY ListVariantX IS
  List_Variant(Refinement(FileSystem_r))==(0)
END
&
THEORY ListAssertionsX IS
  Expanded_List_Assertions(Refinement(FileSystem_r))==(btrue);
  Abstract_List_Assertions(Refinement(FileSystem_r))==(btrue);
  Context_List_Assertions(Refinement(FileSystem_r))==(btrue);
  List_Assertions(Refinement(FileSystem_r))==(btrue)
END
&
THEORY ListCoverageX IS
  List_Coverage(Refinement(FileSystem_r))==(btrue)
END
&
THEORY ListExclusivityX IS
  List_Exclusivity(Refinement(FileSystem_r))==(btrue)
END
&
THEORY ListInitialisationX IS
  Expanded_List_Initialisation(Refinement(FileSystem_r))==(files,directories,objectTree,contents,closed,ropened,wopened,position:={},{root},{root|->root},{},{},{},{},{});
  Context_List_Initialisation(Refinement(FileSystem_r))==(skip);
  List_Initialisation(Refinement(FileSystem_r))==(files,directories,objectTree,contents,closed,ropened,wopened,position:={},{root},{root|->root},{},{},{},{},{})
END
&
THEORY ListOperationsX IS
  Internal_List_Operations(Refinement(FileSystem_r))==(create_file,create_dir,move,copy,delete,ropen,wopen,close,read,write);
  List_Operations(Refinement(FileSystem_r))==(create_file,create_dir,move,copy,delete,ropen,wopen,close,read,write)
END
&
THEORY ListInputX IS
  List_Input(Refinement(FileSystem_r),create_file)==(file,parent_dir);
  List_Input(Refinement(FileSystem_r),create_dir)==(dir,parent_dir);
  List_Input(Refinement(FileSystem_r),move)==(obj,destDir);
  List_Input(Refinement(FileSystem_r),copy)==(obj,destDir);
  List_Input(Refinement(FileSystem_r),delete)==(obj);
  List_Input(Refinement(FileSystem_r),ropen)==(file);
  List_Input(Refinement(FileSystem_r),wopen)==(file);
  List_Input(Refinement(FileSystem_r),close)==(file);
  List_Input(Refinement(FileSystem_r),read)==(file);
  List_Input(Refinement(FileSystem_r),write)==(file,data)
END
&
THEORY ListOutputX IS
  List_Output(Refinement(FileSystem_r),create_file)==(?);
  List_Output(Refinement(FileSystem_r),create_dir)==(?);
  List_Output(Refinement(FileSystem_r),move)==(?);
  List_Output(Refinement(FileSystem_r),copy)==(?);
  List_Output(Refinement(FileSystem_r),delete)==(?);
  List_Output(Refinement(FileSystem_r),ropen)==(?);
  List_Output(Refinement(FileSystem_r),wopen)==(?);
  List_Output(Refinement(FileSystem_r),close)==(?);
  List_Output(Refinement(FileSystem_r),read)==(byte);
  List_Output(Refinement(FileSystem_r),write)==(?)
END
&
THEORY ListHeaderX IS
  List_Header(Refinement(FileSystem_r),create_file)==(create_file(file,parent_dir));
  List_Header(Refinement(FileSystem_r),create_dir)==(create_dir(dir,parent_dir));
  List_Header(Refinement(FileSystem_r),move)==(move(obj,destDir));
  List_Header(Refinement(FileSystem_r),copy)==(copy(obj,destDir));
  List_Header(Refinement(FileSystem_r),delete)==(delete(obj));
  List_Header(Refinement(FileSystem_r),ropen)==(ropen(file));
  List_Header(Refinement(FileSystem_r),wopen)==(wopen(file));
  List_Header(Refinement(FileSystem_r),close)==(close(file));
  List_Header(Refinement(FileSystem_r),read)==(byte <-- read(file));
  List_Header(Refinement(FileSystem_r),write)==(write(file,data))
END
&
THEORY ListOperationGuardX END
&
THEORY ListPreconditionX IS
  Own_Precondition(Refinement(FileSystem_r),create_file)==(file: OBJECT & file/:files & file/:directories & parent_dir: directories);
  List_Precondition(Refinement(FileSystem_r),create_file)==(file: OBJECT & file/:files & file/:directories & parent_dir: directories & file: OBJECT & file/:files & file/:directories & parent_dir: directories);
  Own_Precondition(Refinement(FileSystem_r),create_dir)==(dir: OBJECT & dir/:files & dir/:directories & parent_dir: directories);
  List_Precondition(Refinement(FileSystem_r),create_dir)==(dir: OBJECT & dir/:files & dir/:directories & parent_dir: directories & dir: OBJECT & dir/:files & dir/:directories & parent_dir: directories);
  Own_Precondition(Refinement(FileSystem_r),move)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & destDir/=obj);
  List_Precondition(Refinement(FileSystem_r),move)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & destDir/=obj & obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & destDir/=obj);
  Own_Precondition(Refinement(FileSystem_r),copy)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree));
  List_Precondition(Refinement(FileSystem_r),copy)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree));
  Own_Precondition(Refinement(FileSystem_r),delete)==(obj: files\/directories & tcl(objectTree~)[{obj}]\/{obj}/\files <: closed);
  List_Precondition(Refinement(FileSystem_r),delete)==(obj: files\/directories & tcl(objectTree~)[{obj}]\/{obj}/\files <: closed & obj: files\/directories & tcl(objectTree~)[{obj}]\/{obj}/\files <: closed);
  Own_Precondition(Refinement(FileSystem_r),ropen)==(file: files & file: closed);
  List_Precondition(Refinement(FileSystem_r),ropen)==(file: files & file: closed & file: files & file: closed);
  Own_Precondition(Refinement(FileSystem_r),wopen)==(file: files & file: closed);
  List_Precondition(Refinement(FileSystem_r),wopen)==(file: files & file: closed & file: files & file: closed);
  Own_Precondition(Refinement(FileSystem_r),close)==(file: files & file: ropened\/wopened);
  List_Precondition(Refinement(FileSystem_r),close)==(file: files & file: ropened\/wopened & file: files & file: ropened\/wopened);
  Own_Precondition(Refinement(FileSystem_r),read)==(file: files & file: ropened & contents(file)/=<>);
  List_Precondition(Refinement(FileSystem_r),read)==(file: files & file: ropened & contents(file)/=<> & file: files & file: ropened & contents(file)/=<>);
  Own_Precondition(Refinement(FileSystem_r),write)==(file: files & file: wopened & data: seq(BYTE));
  List_Precondition(Refinement(FileSystem_r),write)==(file: files & file: wopened & data: seq(BYTE) & file: files & file: wopened & data: seq(BYTE))
END
&
THEORY ListSubstitutionX IS
  Expanded_List_Substitution(Refinement(FileSystem_r),write)==(file: files & file: wopened & data: seq(BYTE) & file: files & file: wopened & data: seq(BYTE) | contents:=contents<+{file|->(contents(file)^data)});
  Expanded_List_Substitution(Refinement(FileSystem_r),read)==(file: files & file: ropened & contents(file)/=<> & file: files & file: ropened & contents(file)/=<> | position(file): dom(contents(file)) ==> byte,position:=contents(file)(position(file)),position<+{file|->position(file)+1} [] not(position(file): dom(contents(file))) ==> byte:=eof);
  Expanded_List_Substitution(Refinement(FileSystem_r),close)==(file: files & file: ropened\/wopened & file: files & file: ropened\/wopened | closed,ropened,wopened,position:=closed\/{file},ropened-{file},wopened-{file},{file}<<|position);
  Expanded_List_Substitution(Refinement(FileSystem_r),wopen)==(file: files & file: closed & file: files & file: closed | closed,wopened:=closed-{file},wopened\/{file});
  Expanded_List_Substitution(Refinement(FileSystem_r),ropen)==(file: files & file: closed & file: files & file: closed | closed,ropened,position:=closed-{file},ropened\/{file},position<+{file|->1});
  Expanded_List_Substitution(Refinement(FileSystem_r),delete)==(obj: files\/directories & tcl(objectTree~)[{obj}]\/{obj}/\files <: closed & obj: files\/directories & tcl(objectTree~)[{obj}]\/{obj}/\files <: closed | @to_delete.(to_delete = tcl(objectTree~)[{obj}]\/{obj} ==> files,closed,directories,objectTree,contents:=files-to_delete,closed-to_delete,directories-to_delete,to_delete<<|objectTree|>>to_delete,to_delete<<|contents));
  Expanded_List_Substitution(Refinement(FileSystem_r),copy)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) | @cp.(cp: OBJECT & cp/:(files\/directories) ==> (obj: files ==> files,objectTree,contents,closed:=files\/{cp},objectTree<+{cp|->destDir},contents<+{cp|->contents(obj)},closed\/{cp} [] not(obj: files) ==> directories,objectTree:=directories\/{cp},objectTree<+{cp|->destDir})));
  Expanded_List_Substitution(Refinement(FileSystem_r),move)==(obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & destDir/=obj & obj: files\/directories & destDir: directories & destDir|->obj/:tcl(objectTree) & destDir/=obj | objectTree:=objectTree<+{obj|->destDir});
  Expanded_List_Substitution(Refinement(FileSystem_r),create_dir)==(dir: OBJECT & dir/:files & dir/:directories & parent_dir: directories & dir: OBJECT & dir/:files & dir/:directories & parent_dir: directories | directories,objectTree:=directories\/{dir},objectTree<+{dir|->parent_dir});
  Expanded_List_Substitution(Refinement(FileSystem_r),create_file)==(file: OBJECT & file/:files & file/:directories & parent_dir: directories & file: OBJECT & file/:files & file/:directories & parent_dir: directories | files,objectTree,closed,contents:=files\/{file},objectTree<+{file|->parent_dir},closed\/{file},contents<+{file|-><>});
  List_Substitution(Refinement(FileSystem_r),create_file)==(files:=files\/{file} || objectTree:=objectTree<+{file|->parent_dir} || closed:=closed\/{file} || contents(file):=<>);
  List_Substitution(Refinement(FileSystem_r),create_dir)==(directories:=directories\/{dir} || objectTree:=objectTree<+{dir|->parent_dir});
  List_Substitution(Refinement(FileSystem_r),move)==(objectTree:=objectTree<+{obj|->destDir});
  List_Substitution(Refinement(FileSystem_r),copy)==(ANY cp WHERE cp: OBJECT & cp/:(files\/directories) THEN IF obj: files THEN files:=files\/{cp} || objectTree(cp):=destDir || contents(cp):=contents(obj) || closed:=closed\/{cp} ELSE directories:=directories\/{cp} || objectTree(cp):=destDir END END);
  List_Substitution(Refinement(FileSystem_r),delete)==(LET to_delete BE to_delete = tcl(objectTree~)[{obj}]\/{obj} IN files:=files-to_delete || closed:=closed-to_delete || directories:=directories-to_delete || objectTree:=to_delete<<|objectTree|>>to_delete || contents:=to_delete<<|contents END);
  List_Substitution(Refinement(FileSystem_r),ropen)==(closed:=closed-{file} || ropened:=ropened\/{file} || position(file):=1);
  List_Substitution(Refinement(FileSystem_r),wopen)==(closed:=closed-{file} || wopened:=wopened\/{file});
  List_Substitution(Refinement(FileSystem_r),close)==(closed:=closed\/{file} || ropened:=ropened-{file} || wopened:=wopened-{file} || position:={file}<<|position);
  List_Substitution(Refinement(FileSystem_r),read)==(IF position(file): dom(contents(file)) THEN byte:=contents(file)(position(file)) || position(file):=position(file)+1 ELSE byte:=eof END);
  List_Substitution(Refinement(FileSystem_r),write)==(contents(file):=contents(file)^data)
END
&
THEORY ListParametersX IS
  List_Parameters(Refinement(FileSystem_r))==(?)
END
&
THEORY ListInstanciatedParametersX END
&
THEORY ListConstraintsX IS
  List_Constraints(Refinement(FileSystem_r))==(btrue);
  List_Context_Constraints(Refinement(FileSystem_r))==(btrue)
END
&
THEORY ListConstantsX IS
  List_Valuable_Constants(Refinement(FileSystem_r))==(root,tcl,eof);
  Inherited_List_Constants(Refinement(FileSystem_r))==(root,tcl,eof);
  List_Constants(Refinement(FileSystem_r))==(?)
END
&
THEORY ListSetsX IS
  Set_Definition(Refinement(FileSystem_r),BYTE)==(?);
  Context_List_Enumerated(Refinement(FileSystem_r))==(?);
  Context_List_Defered(Refinement(FileSystem_r))==(?);
  Context_List_Sets(Refinement(FileSystem_r))==(?);
  List_Valuable_Sets(Refinement(FileSystem_r))==(OBJECT,BYTE);
  Inherited_List_Enumerated(Refinement(FileSystem_r))==(?);
  Inherited_List_Defered(Refinement(FileSystem_r))==(OBJECT,BYTE);
  Inherited_List_Sets(Refinement(FileSystem_r))==(OBJECT,BYTE);
  List_Enumerated(Refinement(FileSystem_r))==(?);
  List_Defered(Refinement(FileSystem_r))==(?);
  List_Sets(Refinement(FileSystem_r))==(?);
  Set_Definition(Refinement(FileSystem_r),OBJECT)==(?)
END
&
THEORY ListHiddenConstantsX IS
  Abstract_List_HiddenConstants(Refinement(FileSystem_r))==(?);
  Expanded_List_HiddenConstants(Refinement(FileSystem_r))==(?);
  List_HiddenConstants(Refinement(FileSystem_r))==(?);
  External_List_HiddenConstants(Refinement(FileSystem_r))==(?)
END
&
THEORY ListPropertiesX IS
  Abstract_List_Properties(Refinement(FileSystem_r))==(root: OBJECT & eof: BYTE & tcl: OBJECT <-> OBJECT --> (OBJECT <-> OBJECT) & !rr.(rr: OBJECT <-> OBJECT => rr <: tcl(rr)) & !rr.(rr: OBJECT <-> OBJECT => (rr;tcl(rr)) <: tcl(rr)) & OBJECT: FIN(INTEGER) & not(OBJECT = {}) & BYTE: FIN(INTEGER) & not(BYTE = {}));
  Context_List_Properties(Refinement(FileSystem_r))==(btrue);
  Inherited_List_Properties(Refinement(FileSystem_r))==(btrue);
  List_Properties(Refinement(FileSystem_r))==(btrue)
END
&
THEORY ListSeenInfoX END
&
THEORY ListANYVarX IS
  List_ANY_Var(Refinement(FileSystem_r),create_file)==(?);
  List_ANY_Var(Refinement(FileSystem_r),create_dir)==(?);
  List_ANY_Var(Refinement(FileSystem_r),move)==(?);
  List_ANY_Var(Refinement(FileSystem_r),copy)==(Var(cp) == atype(OBJECT,?,?));
  List_ANY_Var(Refinement(FileSystem_r),delete)==(?);
  List_ANY_Var(Refinement(FileSystem_r),ropen)==(?);
  List_ANY_Var(Refinement(FileSystem_r),wopen)==(?);
  List_ANY_Var(Refinement(FileSystem_r),close)==(?);
  List_ANY_Var(Refinement(FileSystem_r),read)==(?);
  List_ANY_Var(Refinement(FileSystem_r),write)==(?)
END
&
THEORY ListOfIdsX IS
  List_Of_Ids(Refinement(FileSystem_r)) == (? | ? | ? | ? | create_file,create_dir,move,copy,delete,ropen,wopen,close,read,write | ? | ? | ? | FileSystem_r);
  List_Of_HiddenCst_Ids(Refinement(FileSystem_r)) == (? | ?);
  List_Of_VisibleCst_Ids(Refinement(FileSystem_r)) == (?);
  List_Of_VisibleVar_Ids(Refinement(FileSystem_r)) == (position,wopened,ropened,closed,contents,directories,files,objectTree | ?);
  List_Of_Ids_SeenBNU(Refinement(FileSystem_r)) == (?: ?)
END
&
THEORY SetsEnvX IS
  Sets(Refinement(FileSystem_r)) == (Type(BYTE) == Cst(SetOf(atype(BYTE,"[BYTE","]BYTE")));Type(OBJECT) == Cst(SetOf(atype(OBJECT,"[OBJECT","]OBJECT"))))
END
&
THEORY ConstantsEnvX IS
  Constants(Refinement(FileSystem_r)) == (Type(eof) == Cst(atype(BYTE,?,?));Type(tcl) == Cst(SetOf(SetOf(atype(OBJECT,?,?)*atype(OBJECT,?,?))*SetOf(atype(OBJECT,?,?)*atype(OBJECT,?,?))));Type(root) == Cst(atype(OBJECT,?,?)))
END
&
THEORY VisibleVariablesEnvX IS
  VisibleVariables(Refinement(FileSystem_r)) == (Type(position) == Mvv(SetOf(atype(OBJECT,?,?)*btype(INTEGER,1,MAXINT)));Type(wopened) == Mvv(SetOf(atype(OBJECT,?,?)));Type(ropened) == Mvv(SetOf(atype(OBJECT,?,?)));Type(closed) == Mvv(SetOf(atype(OBJECT,?,?)));Type(contents) == Mvv(SetOf(atype(OBJECT,?,?)*SetOf(btype(INTEGER,?,?)*atype(BYTE,?,?))));Type(directories) == Mvv(SetOf(atype(OBJECT,?,?)));Type(files) == Mvv(SetOf(atype(OBJECT,?,?)));Type(objectTree) == Mvv(SetOf(atype(OBJECT,?,?)*atype(OBJECT,?,?))))
END
&
THEORY OperationsEnvX IS
  Operations(Refinement(FileSystem_r)) == (Type(write) == Cst(No_type,atype(OBJECT,?,?)*SetOf(btype(INTEGER,?,?)*atype(BYTE,?,?)));Type(read) == Cst(atype(BYTE,?,?),atype(OBJECT,?,?));Type(close) == Cst(No_type,atype(OBJECT,?,?));Type(wopen) == Cst(No_type,atype(OBJECT,?,?));Type(ropen) == Cst(No_type,atype(OBJECT,?,?));Type(delete) == Cst(No_type,atype(OBJECT,?,?));Type(copy) == Cst(No_type,atype(OBJECT,?,?)*atype(OBJECT,?,?));Type(move) == Cst(No_type,atype(OBJECT,?,?)*atype(OBJECT,?,?));Type(create_dir) == Cst(No_type,atype(OBJECT,?,?)*atype(OBJECT,?,?));Type(create_file) == Cst(No_type,atype(OBJECT,?,?)*atype(OBJECT,?,?)))
END
&
THEORY TCIntRdX IS
  predB0 == OK;
  extended_sees == KO;
  B0check_tab == KO;
  local_op == OK;
  abstract_constants_visible_in_values == KO;
  project_type == SOFTWARE_TYPE;
  event_b_deadlockfreeness == KO;
  variant_clause_mandatory == KO;
  event_b_coverage == KO;
  event_b_exclusivity == KO;
  genFeasibilityPO == KO
END
)
